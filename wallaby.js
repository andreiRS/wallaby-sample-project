module.exports = function () {
  return {
    files: [
      'models/*.js'
    ],

    tests: [
      'test/*Spec.js'
    ],

    env: {
      type: 'node'
    }
  };
};