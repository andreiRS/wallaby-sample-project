var assert = require("assert");
var Calculator = require("../models/calculator");

describe('Calculator', function () {
    beforeEach(function () {
        this.calculator = new Calculator();
    });

    describe('add method', function () {
        it('should return 3 when adding 1 and 2', function () {
            assert.equal(3, this.calculator.add(1, 2));
        })
    });

    describe('subtract method', function () {
        it('should return 1 when subtracting 2 out of 3', function () {
            assert.equal(1, this.calculator.subtract(3, 2));
        })
    })
});
